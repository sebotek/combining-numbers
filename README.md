# README #

### Uruchomienie projektu ###

* w wersji dla maven `mvn spring-boot:run`
* w wersji dla gradle `./gradlew bootRun `
* następnie należy otworzyć przeglądarke pod adresem `http://localhost:8080/` oraz wybrać sposób działania i obserwować efekt 

### Autor ###
Sebastian Kociński
