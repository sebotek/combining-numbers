package com.kocinski.sebastian.combiningnumbers.service;

import com.kocinski.sebastian.combiningnumbers.datasource.DataSource;
import org.springframework.stereotype.Service;


@Service("randomorgservice")
public class RandomOrgService implements DataService<Integer> {
    private final DataSource<String> randomOrgDataSource;

    public RandomOrgService(DataSource<String> ds) {
        this.randomOrgDataSource = ds;
    }

    @Override
    public Integer getNumber() {
        String data = randomOrgDataSource.getData();
        return Integer.parseInt(data.trim());
    }
}
