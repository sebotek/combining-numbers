package com.kocinski.sebastian.combiningnumbers.service;

import com.kocinski.sebastian.combiningnumbers.datasource.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("databaseservice")
public class DatabaseService implements DataService<Integer> {
    private final DataSource<Integer> dataSource;

    public DatabaseService(@Qualifier("databasesource") DataSource<Integer> ds) {
        dataSource = ds;
    }

    @Override
    public Integer getNumber() {
        return dataSource.getData();
    }
}
