package com.kocinski.sebastian.combiningnumbers.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class StrategyContext {

    private final Map<String, DataService<Integer>> context = new HashMap<>();

    public StrategyContext(@Qualifier("databaseservice") DataService<Integer> database,
                           @Qualifier("randomorgservice") DataService<Integer> randomOrg,
                           @Qualifier("javarandomservice") DataService<Integer> javarandom) {
        context.put("database", database);
        context.put("randomorg", randomOrg);
        context.put("javarandom", javarandom);

    }

    public DataService<Integer> getStrategy(String str) {
        return context.get(str);
    }
}
