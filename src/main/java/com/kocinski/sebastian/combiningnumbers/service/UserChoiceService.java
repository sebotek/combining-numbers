package com.kocinski.sebastian.combiningnumbers.service;

import com.kocinski.sebastian.combiningnumbers.dto.CombiningResultDTO;
import com.kocinski.sebastian.combiningnumbers.dto.UserChoiceDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserChoiceService {
    private final StrategyContext strategyContext;
    private final CombiningService<DataService<Integer>, DataService<Integer>, CombiningResultDTO> combiningService;

    public CombiningResultDTO getResult(UserChoiceDTO userChoiceDTO) {
        DataService<Integer> strategy1 = strategyContext.getStrategy(userChoiceDTO.getSource1());
        DataService<Integer> strategy2 = strategyContext.getStrategy(userChoiceDTO.getSource2());
        return combiningService.combine(strategy1, strategy2);
    }
}
