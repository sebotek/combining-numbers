package com.kocinski.sebastian.combiningnumbers.service;

import com.kocinski.sebastian.combiningnumbers.dto.CombiningResultDTO;
import org.springframework.stereotype.Service;

@Service
public class IntegerCombiningService implements CombiningService<DataService<Integer>, DataService<Integer>, CombiningResultDTO> {

    @Override
    public CombiningResultDTO combine(DataService<Integer> dataService1, DataService<Integer> dataService2) {
        Integer number1 = dataService1.getNumber();
        Integer number2 = dataService1.getNumber();
        return CombiningResultDTO.builder()
                .argument1(number1)
                .argument2(number2)
                .result(number1 + number2)
                .build();

    }
}
