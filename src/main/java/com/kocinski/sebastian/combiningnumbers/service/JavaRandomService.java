package com.kocinski.sebastian.combiningnumbers.service;

import com.kocinski.sebastian.combiningnumbers.configuration.JavaRandomProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Random;


@Service("javarandomservice")
@RequiredArgsConstructor
public class JavaRandomService implements DataService<Integer> {
    private final JavaRandomProperties javaRandomProperties;
    private final Random random;

    @Override
    public Integer getNumber() {
        return javaRandomProperties.getMinNumber() + random.nextInt(javaRandomProperties.getMaxNumber() + 1);
    }

}
