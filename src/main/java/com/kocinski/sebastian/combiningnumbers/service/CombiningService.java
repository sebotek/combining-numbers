package com.kocinski.sebastian.combiningnumbers.service;

public interface CombiningService<A, B, C> {
    C combine(A dataService1, B dataService2);
}
