package com.kocinski.sebastian.combiningnumbers.service;

public interface DataService<T> {
    T getNumber();
}
