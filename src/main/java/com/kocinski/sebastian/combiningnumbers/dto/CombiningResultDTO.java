package com.kocinski.sebastian.combiningnumbers.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class CombiningResultDTO {
    private Integer argument1;
    private Integer argument2;
    private Integer result;
}
