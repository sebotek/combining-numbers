package com.kocinski.sebastian.combiningnumbers.dto;

import lombok.Data;

@Data
public class UserChoiceDTO {
    private String source1;
    private String source2;
}
