package com.kocinski.sebastian.combiningnumbers.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

@Component
public class BeansConfiguration {
    @Bean
    public RestTemplate restTemplateFactory() {
        return new RestTemplate();
    }

    @Bean
    public Random randomFactory() {
        return new Random();
    }
}
