package com.kocinski.sebastian.combiningnumbers.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = "classpath:application.yml")
@ConfigurationProperties(prefix = "javarandom")
@Getter
@Setter
public class JavaRandomProperties {
    private int minNumber;
    private int maxNumber;
}
