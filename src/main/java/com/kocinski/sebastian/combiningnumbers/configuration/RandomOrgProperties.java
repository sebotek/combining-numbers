package com.kocinski.sebastian.combiningnumbers.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = "classpath:application.yml")
@ConfigurationProperties(prefix = "randomorg")
@Getter
@Setter
public class RandomOrgProperties {
    private int minNumber;
    private int maxNumber;
}
