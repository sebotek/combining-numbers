package com.kocinski.sebastian.combiningnumbers.controller;

import com.kocinski.sebastian.combiningnumbers.dto.CombiningResultDTO;
import com.kocinski.sebastian.combiningnumbers.dto.UserChoiceDTO;
import com.kocinski.sebastian.combiningnumbers.service.UserChoiceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
@Slf4j
public class CombiningController {
    private final UserChoiceService userChoiceService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String combine(Model model) {
        try {
            model.addAttribute("UserChoice", new UserChoiceDTO());
            return "index.html";
        } catch (RuntimeException e) {
            e.printStackTrace();
            log.error("Exception:" + e.getMessage());
            return "error.html";
        }
    }

    @RequestMapping(value = "/combining", method = RequestMethod.POST)
    public String combining(UserChoiceDTO userChoiceDTO, Model model) {
        try {
            model.addAttribute("UserChoice", new UserChoiceDTO());
            CombiningResultDTO result = userChoiceService.getResult(userChoiceDTO);
            model.addAttribute("result", result);
            return "result.html";
        } catch (RuntimeException e) {
            e.printStackTrace();
            log.error("Exception:" + e.getMessage());
            return "error.html";
        }
    }

}
