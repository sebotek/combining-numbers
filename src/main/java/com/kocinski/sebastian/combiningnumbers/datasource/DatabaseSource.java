package com.kocinski.sebastian.combiningnumbers.datasource;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;


@Service("databasesource")
@RequiredArgsConstructor
public class DatabaseSource implements DataSource<Integer> {
    private final JdbcTemplate jdbcTemplate;
    private static final String SELECT_NUMBER = "select number from numbers limit 1";

    @Override
    public Integer getData() {
        return jdbcTemplate.queryForObject(SELECT_NUMBER, Integer.class);
    }
}
