package com.kocinski.sebastian.combiningnumbers.datasource;


public interface DataSource<T> {
    T getData();
}
