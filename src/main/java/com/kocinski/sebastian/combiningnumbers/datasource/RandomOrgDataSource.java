package com.kocinski.sebastian.combiningnumbers.datasource;

import com.kocinski.sebastian.combiningnumbers.configuration.RandomOrgProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service("randomOrg")
@RequiredArgsConstructor
public class RandomOrgDataSource implements DataSource<String> {
    private final RestTemplate restTemplate;
    private final RandomOrgProperties randomOrgProperties;

    private static final String URL = "https://www.random.org/integers/?num=1&min={minNumber}&max={maxNumber}&col=" +
            "1&base=10&format=plain&rnd=new&col=1&format=plain";

    @Override
    public String getData() {
        ResponseEntity<String> entity = restTemplate.getForEntity(URL, String.class,
                randomOrgProperties.getMinNumber(), randomOrgProperties.getMaxNumber());
        return entity.getBody();
    }

}
