package com.kocinski.sebastian.combiningnumbers.service;

import com.kocinski.sebastian.combiningnumbers.dto.CombiningResultDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class IntegerCombiningServiceTest {

    @Autowired
    private CombiningService<DataService<Integer>, DataService<Integer>, CombiningResultDTO> combiningService;
    @Autowired
    private StrategyContext strategyContext;

    @Test
    public void combiningNumber_CombiningServiceCallCombine_resultIsNumberGreatherThan0() {
        DataService<Integer> javarandom = strategyContext.getStrategy("javarandom");
        DataService<Integer> databaseservice = strategyContext.getStrategy("database");
        CombiningResultDTO combine = combiningService.combine(javarandom, databaseservice);
        Assertions.assertTrue(combine.getResult() > 0);
    }
}
