package com.kocinski.sebastian.combiningnumbers.service;

import com.kocinski.sebastian.combiningnumbers.configuration.JavaRandomProperties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Random;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@SpringBootTest
public class JavaRandomServiceTest {
    @Autowired
    @Qualifier("javarandomservice")
    private DataService<Integer> javaRandomService;

    private static JavaRandomProperties javaRandomProperties;

    @BeforeAll
    public static void beforeAll() {
        javaRandomProperties = new JavaRandomProperties();
        javaRandomProperties.setMaxNumber(11);
        javaRandomProperties.setMinNumber(1);

    }


    @Test
    public void getNumber_JavaRandom_someNumberNotNull() {
        Integer number = javaRandomService.getNumber();
        Assertions.assertNotNull(number);
    }

    @Test
    public void getNumber_JavaRandomGets0_getBeginOfRange() {
        //given
        Random random = Mockito.mock(Random.class);
        JavaRandomService javaRandomService = new JavaRandomService(javaRandomProperties, random);
        //when
        doReturn(0).when(random).nextInt(anyInt());
        Integer result = javaRandomService.getNumber();
        //then
        Assertions.assertEquals(1, result);
    }

    @Test
    public void getNumber_JavaRandomGets10_getEndOfRange() {
        //given
        Random random = Mockito.mock(Random.class);
        JavaRandomService javaRandomService = new JavaRandomService(javaRandomProperties, random);
        //when
        doReturn(10).when(random).nextInt(anyInt());
        Integer result = javaRandomService.getNumber();
        //then
        Assertions.assertEquals(11, result);

    }
}
