package com.kocinski.sebastian.combiningnumbers.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DatabaseServiceTest {
    @Autowired
    @Qualifier("databaseservice")
    DataService<Integer> databaseService;

    @Test
    public void getNumber_queryToDatabase_getNumber4321() {
        Integer number = databaseService.getNumber();
        Assertions.assertEquals(4321, number);
    }
}
