package com.kocinski.sebastian.combiningnumbers.service;

import com.kocinski.sebastian.combiningnumbers.configuration.RandomOrgProperties;
import com.kocinski.sebastian.combiningnumbers.datasource.RandomOrgDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
public class RandomOrgServiceTest {

    @Autowired
    @Qualifier("randomorgservice")
    private DataService<Integer> dataService;

    @Autowired
    private RandomOrgProperties randomOrgProperties;

    @Mock
    RestTemplate restTemplate;

    @BeforeEach
    public void before() {
        RandomOrgDataSource randomOrgDataSource = new RandomOrgDataSource(restTemplate, randomOrgProperties);
        dataService = new RandomOrgService(randomOrgDataSource);
    }

    @Test
    public void getNumber_randomOrgCalled_getNumber1234() {
        //when
        when(restTemplate.getForEntity(any(), any(), any(), any())).thenReturn(ResponseEntity.ok("1234"));
        //then
        Integer result = dataService.getNumber();
        Assertions.assertEquals(1234, result);
    }
}
