package com.kocinski.sebastian.combiningnumbers.datasource;

import com.kocinski.sebastian.combiningnumbers.datasource.DataSource;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Ignore
public class RandomOrgDataSourceTest {

    @Autowired
    @Qualifier("randomOrg")
    private DataSource<String> dataSource;

    @Test
    public void getData_randomOrgCalled_randomNumber() {
        String data = dataSource.getData();
        Assertions.assertNotNull(data);
    }
}
